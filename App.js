
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Provider } from 'react-redux'
import store from './store';
import Forecast from './src/components/Forecast';

export default function App() {
  return (
    <Provider store={store}>
      <Forecast />
    </Provider>


  );
}




// déplacer les balises text dans le component Forecast 
// export default function App() {
//   return (
//     <View style={styles.container}>
//       <Text>KIKOU</Text>
//       <StatusBar style="auto" />
//     </View>
//   );
// }











// //app.js en react native est le point d'entrée
// //le texte doit toujours etre ds des balises text
// mettre toutes les balises html en balises text ou image selon la doc ReactNative