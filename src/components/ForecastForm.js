import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from "react-redux";
import { fetchForecast } from '../actions/forecast';
import loading from '../loading.gif';
import { StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native';


class ForecastForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            'city': 'Lyon',
        }
    }

    changeCity = (event) => {
        this.setState({
            'city': event.target.value
        })
    }

    fetchForecast = () => {
        this.props.fetchForecast(this.state.city);
    }

    render() {
        return (
            <View style={styles.container}>
                <Text>
                    <div>
                        <TextInput onChangeText={this.changeCity} value={this.state.city} type="text" />
                        {
                            this.props.loader === false ?
                                <TouchableOpacity onPress={this.fetchForecast}>Rechercher</TouchableOpacity>

                                :
                                <div>
                                    <Image source={loading} className="App-loading" alt="loading" />
                    LOADING</div>
                        }
                    </div>
                </Text>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});

const mapStateToProps = (state /*, ownProps */) => {
    return {
        loader: state.forecastReducer.loader
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        fetchForecast,
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(ForecastForm)