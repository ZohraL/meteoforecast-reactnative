import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import ForecastChooseCity from './ForecastChooseCity';

class ForecastTitle extends React.Component {

    render() {
        return (
            <View style={styles.container}>
                <Text>

                    <div>

                        <ForecastChooseCity />
                        <Text>
                            <h2>DATE : Samedi 6 mars 2021</h2>
                        </Text>
                    </div>
                </Text>
            </View>
        );
    }

}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});

export default ForecastTitle;