import React from 'react';
import ForecastResult from './ForecastResult';
import ForecastTitle from './ForecastTitle';
import ForecastForm from './ForecastForm';
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';

function Forecast() {
    return (


        <View style={styles.container}>
            <Text>
                <div>
                    <ForecastTitle />
                    <ForecastResult />
                    <ForecastForm />
                </div>
            </Text>
            <StatusBar style="auto" />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});

export default Forecast;